FROM openjdk:11.0.14
EXPOSE 9000
ADD target/droneapi.jar droneapi.jar
ENTRYPOINT ["java","-jar","/droneapi.jar"]
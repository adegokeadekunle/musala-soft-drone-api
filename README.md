# Musala Soft Drone Api Task

---

:scroll: **START**

[//]: # (##Build commands)

[//]: # (- **Docker build** : docker build -t droneapi.jar .)

[//]: # (- **Docker run** : docker run -p 9090:9000 droneapi.jar)

##Links
- **swagger ui** : http://localhost:9000/musala-soft.html
- **H2 console** : http://localhost:9000/h2-console
- **server port** : http://localhost:9000
- **Gitlab link** : https://gitlab.com/adegokeadekunle/musala-soft-drone-api.git

##Build Test Run
- **Build and run test** : mvn clean install
- **Docker build** : docker build -t droneapi.jar .

##Ports
- **server.port**= 9000

##Database
- **database path**= /h2-console
- **database url** = jdbc:h2:file:~/h2/droneDb
- **database username** = sa
- **database password** = 
- **spring.jpa.hibernate.ddl-auto** = create-drop

##Payloads
**Drone :**
- POST : /api/v1/drone/register-drone 
  {
    "model": "LIGHT_WEIGHT"
   }

- POST : /api/v1/drone/load-drone 
         KEY: drone-id , VALUE :  
         KEY: med-id , VALUE : 
- GET : /api/v1/drone/get-medication-on-drone
         KEY: drone-id , VALUE :
- GET : /api/v1/drone/get-drone
         KEY: drone-id , VALUE :
- GET : /api/v1/drone/get-all-drones
- GET : /api/v1/drone/drone-battery-level
         KEY: drone-id , VALUE :
- GET : /api/v1/drone/available-drones-for-loading

**Medication**
- POST : /api/v1/medication/register 
  {
  "name": "string",
  "weight": 0,
  "image": "string"
  }
- GET : /api/v1/medication/get-medication 
         KEY: med_id , VALUE :
- GET : /api/v1/medication/get-all-medication


:scroll: **END**


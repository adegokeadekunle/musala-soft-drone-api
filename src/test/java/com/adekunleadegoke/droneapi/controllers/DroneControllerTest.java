package com.adekunleadegoke.droneapi.controllers;

import com.adekunleadegoke.droneapi.enums.DroneModels;
import com.adekunleadegoke.droneapi.payloads.requests.DroneRequest;
import com.adekunleadegoke.droneapi.services.DroneService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {DroneController.class})
@ExtendWith(SpringExtension.class)
class DroneControllerTest {

    @Autowired
    private DroneController droneController;

    @MockBean
    private DroneService droneService;

    @Test
    void testShouldRegisterDrone() throws Exception {
        when(droneService.registerDrone((DroneRequest) any())).thenReturn(new ResponseEntity<>(HttpStatus.CREATED));

        DroneRequest droneRequest = new DroneRequest();
        droneRequest.setModel(DroneModels.LIGHT_WEIGHT);
        String content = (new ObjectMapper()).writeValueAsString(droneRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/v1/drone/register-drone")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions resultAction = MockMvcBuilders.standaloneSetup(droneController)
                .build()
                .perform(requestBuilder);
        resultAction.andExpect(MockMvcResultMatchers.status().is(201));
    }


    @Test
    void shouldGetAllDrones() throws Exception {
        when(droneService.getAllDrones()).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/drone/get-all-drones");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(droneController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }


    @Test
    void shouldGetAvailableDronesForLoading() throws Exception {
        when(droneService.getAvailableDronesForLoading()).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/v1/drone/available-drones-for-loading");
        ResultActions resultAction = MockMvcBuilders.standaloneSetup(droneController)
                .build()
                .perform(requestBuilder);
        resultAction.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void shouldGetDroneBatteryLevel() throws Exception {
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/drone/drone-battery-level");
        MockHttpServletRequestBuilder requestBuilder = getResult.param("droneId", String.valueOf(1L));
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(droneController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }


    @Test
    void shouldGetDrones() throws Exception {
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/drone/get-drone");
        MockHttpServletRequestBuilder requestBuilder = getResult.param("id", String.valueOf(1L));
        ResultActions resultAction = MockMvcBuilders.standaloneSetup(droneController)
                .build()
                .perform(requestBuilder);
        resultAction.andExpect(MockMvcResultMatchers.status().is(400));
    }


    @Test
    void shouldGetMedicationItemsOnDrone() throws Exception {
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/drone/get-medication-on-drone");
        MockHttpServletRequestBuilder requestBuilder = getResult.param("droneId", String.valueOf(1L));
        ResultActions resultAction = MockMvcBuilders.standaloneSetup(droneController)
                .build()
                .perform(requestBuilder);
        resultAction.andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void shouldLoadDroneWithMedicationItems() throws Exception {
        MockHttpServletRequestBuilder postResult = MockMvcRequestBuilders.post("/api/v1/drone/load-drone");
        MockHttpServletRequestBuilder paramResult = postResult.param("droneId", String.valueOf(1L));
        MockHttpServletRequestBuilder requestBuilder = paramResult.param("medicationId", String.valueOf(1L));
        ResultActions resultAction = MockMvcBuilders.standaloneSetup(droneController)
                .build()
                .perform(requestBuilder);
        resultAction.andExpect(MockMvcResultMatchers.status().is(400));
    }
}
package com.adekunleadegoke.droneapi.services.serviceImpl;

import com.adekunleadegoke.droneapi.enums.DroneModels;
import com.adekunleadegoke.droneapi.enums.DroneStates;
import com.adekunleadegoke.droneapi.exceptions.MedicationErrorException;
import com.adekunleadegoke.droneapi.models.Drone;
import com.adekunleadegoke.droneapi.models.Medication;
import com.adekunleadegoke.droneapi.payloads.requests.MedicationRequest;
import com.adekunleadegoke.droneapi.repositories.MedicationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {MedicationServiceImpl.class})
@ExtendWith(SpringExtension.class)
class MedicationServiceImplTest {

    @MockBean
    private MedicationRepository medicationRepository;

    @Autowired
    private MedicationServiceImpl medicationServiceImp;

    private Medication medication;

    @BeforeEach
    void setUp() {

        Drone drone = new Drone();
        drone.setBatteryPercentage(100);
        drone.setDroneId(1L);
        drone.setDroneModel(DroneModels.LIGHT_WEIGHT);
        drone.setDroneState(DroneStates.IDLE);
        drone.setDroneWeigh(100.0);
        drone.setMedications(new ArrayList<>());
        drone.setSerialNumber("42-THU-SUS-94II4JJ-35-23");

        Medication medication = new Medication();
        medication.setCode("FGYH-1234");
        medication.setDrone(drone);
        medication.setImage("http://image.png");
        medication.setMedId(1L);
        medication.setName("Paracetamol");
        medication.setWeight(20.0d);

    }

    @Test
    void registerMedication() {
        when(medicationRepository.save((Medication) any())).thenReturn(medication);
        ResponseEntity<String> actualRegisterMedicationResult = medicationServiceImp
                .registerMedication(new MedicationRequest());
        assertEquals("medication added to stock successfully.", actualRegisterMedicationResult.getBody());
        assertEquals(HttpStatus.OK, actualRegisterMedicationResult.getStatusCode());
        assertTrue(actualRegisterMedicationResult.getHeaders().isEmpty());
        verify(medicationRepository).save((Medication) any());
        when(medicationRepository.save((Medication) any())).thenThrow(new MedicationErrorException("An error occurred"));
        assertThrows(MedicationErrorException.class,
                () -> medicationServiceImp.registerMedication(new MedicationRequest()));
    }

    @Test
    void getMedication() {

        Drone drone = new Drone();
        drone.setBatteryPercentage(100);
        drone.setDroneId(1L);
        drone.setDroneModel(DroneModels.LIGHT_WEIGHT);
        drone.setDroneState(DroneStates.IDLE);
        drone.setDroneWeigh(100.0);
        drone.setMedications(new ArrayList<>());
        drone.setSerialNumber("42-THU-SUS-94II4JJ-35-23");

        Medication medication = new Medication();
        medication.setCode("FGYH-1234");
        medication.setDrone(drone);
        medication.setImage("http://image.png");
        medication.setMedId(1L);
        medication.setName("Paracetamol");
        medication.setWeight(20.0d);

        Optional<Medication> ofResult = Optional.of(medication);
        when(medicationRepository.findById((Long) any())).thenReturn(ofResult);
        ResponseEntity<Medication> actualMedication = medicationServiceImp.getMedication(1L);
        assertTrue(actualMedication.hasBody());
        assertTrue(actualMedication.getHeaders().isEmpty());
        assertEquals(HttpStatus.OK, actualMedication.getStatusCode());
        verify(medicationRepository).findById((Long) any());

    }

    @Test
    void getAllMedications() {

        ArrayList<Medication> medicationList = new ArrayList<>();
        medicationList.add(medication);
        when(medicationRepository.findAll()).thenReturn(medicationList);
        ResponseEntity<List<Medication>> actualAllMedications = medicationServiceImp.getAllMedications();
        assertTrue(actualAllMedications.hasBody());
        assertEquals(HttpStatus.OK, actualAllMedications.getStatusCode());
        assertTrue(actualAllMedications.getHeaders().isEmpty());
        verify(medicationRepository).findAll();
    }
}
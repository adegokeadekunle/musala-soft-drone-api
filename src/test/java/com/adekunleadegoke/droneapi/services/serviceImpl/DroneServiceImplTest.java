package com.adekunleadegoke.droneapi.services.serviceImpl;

import com.adekunleadegoke.droneapi.enums.DroneModels;
import com.adekunleadegoke.droneapi.enums.DroneStates;
import com.adekunleadegoke.droneapi.exceptions.DroneErrorException;
import com.adekunleadegoke.droneapi.exceptions.MedicationErrorException;
import com.adekunleadegoke.droneapi.models.Drone;
import com.adekunleadegoke.droneapi.models.Medication;
import com.adekunleadegoke.droneapi.payloads.requests.DroneRequest;
import com.adekunleadegoke.droneapi.payloads.responses.DroneResponse;
import com.adekunleadegoke.droneapi.repositories.DroneRepository;
import com.adekunleadegoke.droneapi.repositories.MedicationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ContextConfiguration(classes = {DroneServiceImpl.class})
@ExtendWith(SpringExtension.class)
class DroneServiceImplTest {

    @MockBean
    private DroneRepository droneRepository;

    @Autowired
    private DroneServiceImpl droneServiceImp;

    @MockBean
    private MedicationRepository medicationRepository;

    private Drone drone;

    @BeforeEach
    public void setUp(){
        Drone drone = new Drone();
        drone.setBatteryPercentage(100);
        drone.setDroneId(1L);
        drone.setDroneModel(DroneModels.LIGHT_WEIGHT);
        drone.setDroneState(DroneStates.IDLE);
        drone.setDroneWeigh(100.0);
        drone.setMedications(new ArrayList<>());
        drone.setSerialNumber("42-THU-SUS-94II4JJ-35-23");
    }

    @Test
    void shouldRegisterADrone() {
        when(droneRepository.save((Drone) any())).thenReturn(drone);
        ResponseEntity<DroneResponse> actualRegisterDroneResult = droneServiceImp
                .registerDrone(new DroneRequest(DroneModels.LIGHT_WEIGHT));
        assertTrue(actualRegisterDroneResult.hasBody());
        assertTrue(actualRegisterDroneResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.CREATED, actualRegisterDroneResult.getStatusCode());
        DroneResponse body = actualRegisterDroneResult.getBody();
        assertEquals(DroneModels.LIGHT_WEIGHT, body.getDroneModel());
        assertEquals(100, body.getBatteryPercentage().intValue());
        assertEquals(DroneStates.IDLE, body.getDroneState());
        assertEquals(0.0d, body.getDroneWeigh().doubleValue());
        verify(droneRepository).save((Drone) any());
    }

    @Test
    void shouldFetchADroneWithAnId() {
        Drone drone = new Drone();
        drone.setBatteryPercentage(100);
        drone.setDroneId(1L);
        drone.setDroneModel(DroneModels.LIGHT_WEIGHT);
        drone.setDroneState(DroneStates.IDLE);
        drone.setDroneWeigh(100.0);
        drone.setMedications(new ArrayList<>());
        drone.setSerialNumber("42-THU-SUS-94II4JJ-35-23");
        Optional<Drone> droneOptional = Optional.of(drone);

        when(droneRepository.findById((Long) any())).thenReturn(droneOptional);
        ResponseEntity<Drone> actualDrone = droneServiceImp.getDrone(drone.getDroneId());
        assertTrue(actualDrone.hasBody());
        assertTrue(actualDrone.getHeaders().isEmpty());
        assertEquals(HttpStatus.OK, actualDrone.getStatusCode());
        verify(droneRepository).findById((Long) any());
        when(droneRepository.findById((Long) any())).thenReturn(Optional.empty());
        assertThrows(DroneErrorException.class, () -> droneServiceImp.getDrone(1L));
    }

    @Test
    void wouldGetAllDrones() {

        ArrayList<Drone> droneList = new ArrayList<>();
        droneList.add(drone);
        when(droneRepository.findAll()).thenReturn(droneList);
        ResponseEntity<List<Drone>> actualAllDrones = droneServiceImp.getAllDrones();
        assertTrue(actualAllDrones.hasBody());
        assertEquals(HttpStatus.OK, actualAllDrones.getStatusCode());
        assertTrue(actualAllDrones.getHeaders().isEmpty());
        verify(droneRepository).findAll();

    }

    @Test
    void shouldLoadADroneWithMedicationItems() {

        Drone drone = new Drone();
        drone.setBatteryPercentage(100);
        drone.setDroneId(1L);
        drone.setDroneModel(DroneModels.LIGHT_WEIGHT);
        drone.setDroneState(DroneStates.IDLE);
        drone.setDroneWeigh(100.0);
        drone.setMedications(new ArrayList<>());
        drone.setSerialNumber("42-THU-SUS-94II4JJ-35-23");
        Optional<Drone> droneOptional = Optional.of(drone);

        when(droneRepository.findById((Long) any())).thenReturn(droneOptional);
        when(medicationRepository.findById((Long) any())).thenThrow(new DroneErrorException("An error occurred"));
        assertThrows(DroneErrorException.class, () -> droneServiceImp.loadDroneWithMedicationItems(1L, 1L));
        verify(droneRepository).findById((Long) any());
        verify(medicationRepository).findById((Long) any());
    }

    @Test
    void willGetMedicationItemsOnADrone() {

        Drone drone = mock(Drone.class);
        drone.setBatteryPercentage(100);
        drone.setDroneId(1L);
        drone.setDroneModel(DroneModels.LIGHT_WEIGHT);
        drone.setDroneState(DroneStates.IDLE);
        drone.setDroneWeigh(100.0);
        drone.setMedications(new ArrayList<>());
        drone.setSerialNumber("42-THU-SUS-94II4JJ-35-23");
        Optional<Drone> droneOptional = Optional.of(drone);

        when(droneRepository.findById((Long) any())).thenReturn(droneOptional);
        assertThrows(MedicationErrorException.class, () -> droneServiceImp.getMedicationItemsOnDrone(1L));
        verify(droneRepository).findById((Long) any());
        verify(droneRepository).findById((Long) any());
        verify(drone).getMedications();
        verify(drone).setBatteryPercentage((Integer) any());
        verify(drone).setDroneId((Long) any());
        verify(drone).setDroneModel((DroneModels) any());
        verify(drone).setDroneState((DroneStates) any());
        verify(drone).setDroneWeigh((Double) any());
        verify(drone).setMedications((List<Medication>) any());
        verify(drone).setSerialNumber((String) any());
    }

    @Test
    void WillGetAllAvailableDronesForLoading() {

        Drone drone = new Drone();
        drone.setBatteryPercentage(100);
        drone.setDroneId(1L);
        drone.setDroneModel(DroneModels.LIGHT_WEIGHT);
        drone.setDroneState(DroneStates.IDLE);
        drone.setDroneWeigh(100.0);
        drone.setMedications(new ArrayList<>());
        drone.setSerialNumber("42-THU-SUS-94II4JJ-35-23");
        ArrayList<Drone> droneList = new ArrayList<>();
        droneList.add(drone);

        when(droneRepository.findAll()).thenReturn(droneList);
        ResponseEntity<List<Drone>> actualAvailableDronesForLoading = droneServiceImp.getAvailableDronesForLoading();
        assertEquals(droneList, actualAvailableDronesForLoading.getBody());
        assertEquals(HttpStatus.OK, actualAvailableDronesForLoading.getStatusCode());
        assertTrue(actualAvailableDronesForLoading.getHeaders().isEmpty());
        verify(droneRepository).findAll();
    }

    @Test
    void shouldDisplaysDroneBatteryLevel() {

        Drone drone = new Drone();
        drone.setBatteryPercentage(100);
        drone.setDroneId(1L);
        drone.setDroneModel(DroneModels.LIGHT_WEIGHT);
        drone.setDroneState(DroneStates.IDLE);
        drone.setDroneWeigh(100.0);
        drone.setMedications(new ArrayList<>());
        drone.setSerialNumber("42-THU-SUS-94II4JJ-35-23");
        Optional<Drone> droneOptional = Optional.ofNullable(drone);

        when(droneRepository.findById((Long) any())).thenReturn(droneOptional);
        ResponseEntity<String> actualDroneBatteryLevel = droneServiceImp.getDroneBatteryLevel(1L);
        assertEquals("Drone battery level is 100 percent", actualDroneBatteryLevel.getBody());
        assertEquals(HttpStatus.OK, actualDroneBatteryLevel.getStatusCode());
        assertTrue(actualDroneBatteryLevel.getHeaders().isEmpty());
        verify(droneRepository).findById((Long) any());
    }
}
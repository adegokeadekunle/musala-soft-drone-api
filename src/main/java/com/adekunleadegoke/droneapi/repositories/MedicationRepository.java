package com.adekunleadegoke.droneapi.repositories;

import com.adekunleadegoke.droneapi.models.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationRepository extends JpaRepository<Medication,Long> {

}

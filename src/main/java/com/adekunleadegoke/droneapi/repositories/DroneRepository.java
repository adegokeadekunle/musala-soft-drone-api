package com.adekunleadegoke.droneapi.repositories;

import com.adekunleadegoke.droneapi.models.Drone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DroneRepository extends JpaRepository<Drone,Long> {

}

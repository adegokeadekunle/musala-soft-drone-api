package com.adekunleadegoke.droneapi.preloadedDatabases;

import com.adekunleadegoke.droneapi.enums.DroneModels;
import com.adekunleadegoke.droneapi.enums.DroneStates;
import com.adekunleadegoke.droneapi.models.Drone;
import com.adekunleadegoke.droneapi.models.Medication;

import java.util.ArrayList;
import java.util.List;

public class entityDatabase {

    public static List<Drone> droneDb() {
        List<Drone> drones = new ArrayList<>();
        drones.add(new Drone(1L, "EFRFDJ2334", DroneModels.CRUISER_WEIGHT, 0.0, 45, DroneStates.IDLE, List.of()));
        drones.add(new Drone(2L, "EFRFDJ23e4", DroneModels.LIGHT_WEIGHT, 0.0, 100, DroneStates.IDLE, List.of()));
        drones.add(new Drone(3L, "EFRFDd334", DroneModels.HEAVY_WEIGHT, 0.0, 15, DroneStates.IDLE, List.of()));
        drones.add(new Drone(4L, "EFRFDJ2784", DroneModels.CRUISER_WEIGHT, 0.0, 10, DroneStates.IDLE, List.of()));
        drones.add(new Drone(5L, "EFRFDJ2094", DroneModels.CRUISER_WEIGHT, 0.0, 100, DroneStates.IDLE, List.of()));
        drones.add(new Drone(6L, "EFRFDJ4534", DroneModels.LIGHT_WEIGHT, 0.0, 20, DroneStates.IDLE, List.of()));
        drones.add(new Drone(7L, "EFRFDJ0874", DroneModels.HEAVY_WEIGHT, 0.0, 70, DroneStates.IDLE, List.of()));
        drones.add(new Drone(8L, "EFRFDJ1344", DroneModels.LIGHT_WEIGHT, 0.0, 100, DroneStates.IDLE, List.of()));
        drones.add(new Drone(9L, "EFRFgh5464", DroneModels.MIDDLE_WEIGHT, 0.0, 90, DroneStates.IDLE, List.of()));
        drones.add(new Drone(10L, "EFGFG6534", DroneModels.MIDDLE_WEIGHT, 0.0, 100, DroneStates.IDLE, List.of()));

        return drones;
    }

    public static List<Medication> medicationDb() {
        ArrayList<Medication> medList = new ArrayList<>();
        medList.add(new Medication(1L, "Paracetamol", 20.0, "akkjsk122", "http://image.png", null));
        medList.add(new Medication(2L, "Panadol", 30.0, "akkjsk1WQ", "http://image1.png", null));
        medList.add(new Medication(3L, "Koflin", 60.0, "akkjsk12E", "http://image2.png", null));
        medList.add(new Medication(4L, "Amoxilin", 100.0, "akkjsDWD2", "http://image3.png", null));
        medList.add(new Medication(5L, "Ampiclox", 55.0, "akkjEDF22", "http://image4.png", null));
        medList.add(new Medication(6L, "Tetraciclin", 25.0, "akkFWT122", "http://image5.png", null));
        medList.add(new Medication(7L, "Amlodipil", 35.0, "akkjsk222", "http://image6.png", null));
        medList.add(new Medication(8L, "Amala", 70.0, "akkjsk342", "http://image7.png", null));
        medList.add(new Medication(9L, "Melixin", 10.0, "akkjsk752", "http://image8.png", null));
        medList.add(new Medication(10L, "Iodine", 50.0, "akkjsk862", "http://image9.png", null));
        medList.add(new Medication(11L, "Mentilated Spirit", 40.0, "akkjsk342", "http://image10.png", null));
        medList.add(new Medication(12L, "Ventoline", 80.0, "akkjsk962", "http://image11.png", null));
        medList.add(new Medication(13L, "Inhaler", 60.0, "akkjsk082", "http://image12.png", null));
        medList.add(new Medication(14L, "Sanitizer", 90.0, "akkjsk102", "http://image13.png", null));
        medList.add(new Medication(15L, "Dettol", 110.0, "akkjskD82", "http://image14.png", null));

        return medList;
    }
}

package com.adekunleadegoke.droneapi.enums;

public enum DroneStates {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}

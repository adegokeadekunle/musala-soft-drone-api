package com.adekunleadegoke.droneapi.scheduledTasks;

import com.adekunleadegoke.droneapi.appConstants.Default_Messages;
import com.adekunleadegoke.droneapi.models.Drone;
import com.adekunleadegoke.droneapi.repositories.DroneRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@EnableAsync
@RequiredArgsConstructor
@Slf4j
@Component
public class ScheduledDroneBatteryLevelCheck {
    private final DroneRepository droneRepository;

    @Async
    @Scheduled(fixedRateString = "PT16S", initialDelay = 200L)
    public void scheduleFixedRateTaskAsync()  {
        List<Drone> drones = droneRepository.findAll();
        if(!drones.isEmpty())
            drones
                    .forEach(drone ->
                            log.info(Default_Messages.BATTERY_LEVEL_REPORT, drone.getDroneId(),drone.getDroneModel(), drone.getBatteryPercentage()));
    }

}
package com.adekunleadegoke.droneapi.exceptions;

public class MedicationErrorException extends RuntimeException{
    public MedicationErrorException(String message){
        super(message);
    }
}

package com.adekunleadegoke.droneapi.exceptions;

public class DroneErrorException extends RuntimeException{
    public DroneErrorException(String message){
        super(message);
    }
}
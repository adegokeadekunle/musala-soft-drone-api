package com.adekunleadegoke.droneapi.exceptions;

public class ExcessWeightException extends RuntimeException{

    public ExcessWeightException(String message){
        super(message);
    }
}

package com.adekunleadegoke.droneapi;

import com.adekunleadegoke.droneapi.repositories.DroneRepository;
import com.adekunleadegoke.droneapi.repositories.MedicationRepository;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;

import static com.adekunleadegoke.droneapi.preloadedDatabases.entityDatabase.droneDb;
import static com.adekunleadegoke.droneapi.preloadedDatabases.entityDatabase.medicationDb;

@SpringBootApplication
@EnableScheduling
@OpenAPIDefinition(info = @Info(title = "Drone Api",version="1.0",description="Musala-soft DroneApp api"))
public class DroneMedicalDeliveryApiApplication {

	@Autowired
	private DroneRepository droneRepository;
	@Autowired
	private MedicationRepository medicationRepository;

	@PostConstruct
	public void init(){
		droneRepository.saveAll(droneDb());
		medicationRepository.saveAll(medicationDb());
	}

	public static void main(String[] args) {
		SpringApplication.run(DroneMedicalDeliveryApiApplication.class, args);
	}

}

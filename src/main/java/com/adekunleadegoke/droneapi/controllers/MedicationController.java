package com.adekunleadegoke.droneapi.controllers;

import com.adekunleadegoke.droneapi.models.Medication;
import com.adekunleadegoke.droneapi.payloads.requests.MedicationRequest;
import com.adekunleadegoke.droneapi.services.MedicationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/medication")
@RequiredArgsConstructor
public class MedicationController {
private final MedicationService medicationService;

    @Operation(summary = "This registers a new Medication and add to db")
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> registerMedication(@RequestBody MedicationRequest medicationPayload){
        return medicationService.registerMedication(medicationPayload);
    }

    @Operation(summary = "This will get a Medication with an Id")
    @GetMapping("get-medication")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Medication> getMedication(@RequestParam("med_id")Long medId){
        return medicationService.getMedication(medId);
    }

    @Operation(summary = "This will get all medications")
    @GetMapping("get-all-medication")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Medication>> getAllMedications(){
        return medicationService.getAllMedications();
    }
}

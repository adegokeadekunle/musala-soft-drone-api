package com.adekunleadegoke.droneapi.payloads.responses;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MedicationResponse {

        private String name;
        private String code;
        private Double weight;
        private String image;
}

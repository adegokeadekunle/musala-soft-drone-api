package com.adekunleadegoke.droneapi.payloads.responses;

import com.adekunleadegoke.droneapi.enums.DroneModels;
import com.adekunleadegoke.droneapi.enums.DroneStates;
import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DroneResponse {

    private DroneModels droneModel;
    private DroneStates droneState;
    private String serialNumber;
    private Double droneWeigh;
    private Integer batteryPercentage;
}

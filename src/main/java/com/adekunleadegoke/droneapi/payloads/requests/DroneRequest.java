package com.adekunleadegoke.droneapi.payloads.requests;

import com.adekunleadegoke.droneapi.enums.DroneModels;
import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DroneRequest {

    private DroneModels model;
}

package com.adekunleadegoke.droneapi.payloads.requests;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MedicationRequest {
    private String name;
    private Double weight;
    private String image;
}

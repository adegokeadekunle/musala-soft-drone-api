package com.adekunleadegoke.droneapi.models;

import com.adekunleadegoke.droneapi.enums.DroneModels;
import com.adekunleadegoke.droneapi.enums.DroneStates;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class Drone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long droneId;
    @Size(min = 1, max = 100)
    @NotNull(message = "please enter a serial number for the drone")
    private String serialNumber;
    @Enumerated(EnumType.STRING)
    private DroneModels droneModel;
    @Size(max = 500, message = "total weight capacity exceeded")
    private Double droneWeigh;
    private Integer batteryPercentage;
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "varchar(20) default 'IDLE'")
    private DroneStates droneState;
    @OneToMany(mappedBy = "drone", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JsonIgnore
    List<Medication> medications;

}
package com.adekunleadegoke.droneapi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Pattern;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long medId;
    @Pattern(regexp="^[a-zA-Z0-9_-]$",message="allows only letters, numbers, ‘-‘ and ‘_")
    private String name;
    private Double weight;
    @Pattern(regexp="^[A-Z0-9_]$",message="allows only upper case, numbers and ‘_")
    private String code;
    private String image;
    @ManyToOne
    @JoinColumn(name = "drone_id")
    @JsonIgnore
    private Drone drone;
}

package com.adekunleadegoke.droneapi.services;


import com.adekunleadegoke.droneapi.models.Drone;
import com.adekunleadegoke.droneapi.models.Medication;
import com.adekunleadegoke.droneapi.payloads.requests.DroneRequest;
import com.adekunleadegoke.droneapi.payloads.responses.DroneResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface DroneService {
    ResponseEntity<DroneResponse> registerDrone(DroneRequest droneRequest);
    ResponseEntity<Drone> getDrone(Long id);
    ResponseEntity<List<Drone>> getAllDrones();
    ResponseEntity<String> loadDroneWithMedicationItems(Long droneId, Long medicationId);
    ResponseEntity<List<Medication>> getMedicationItemsOnDrone(Long droneId);
    ResponseEntity<List<Drone>> getAvailableDronesForLoading();
    ResponseEntity<String> getDroneBatteryLevel(Long droneId);
}

package com.adekunleadegoke.droneapi.services.serviceImpl;

import com.adekunleadegoke.droneapi.exceptions.MedicationErrorException;
import com.adekunleadegoke.droneapi.models.Medication;
import com.adekunleadegoke.droneapi.payloads.requests.MedicationRequest;
import com.adekunleadegoke.droneapi.repositories.MedicationRepository;
import com.adekunleadegoke.droneapi.services.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static com.adekunleadegoke.droneapi.appConstants.Default_Messages.*;

@Service
@RequiredArgsConstructor
public class MedicationServiceImpl implements MedicationService {

    private final MedicationRepository medicationRepository;
    @Override
    public ResponseEntity<String> registerMedication(MedicationRequest medicationPayload) {
        Medication medication = Medication.builder()
                .code(UUID.randomUUID().toString())
                .image(medicationPayload.getImage())
                .weight(medicationPayload.getWeight())
                .name(medicationPayload.getName())
                .build();
        medicationRepository.save(medication);
        return ResponseEntity.ok().body(MEDICATION_CREATED_SUCCESS);
    }

    @Override
    public ResponseEntity<Medication> getMedication(Long id) {
        Medication medication = medicationRepository.findById(id).orElseThrow(()-> new MedicationErrorException(MEDICATION_NOT_FOUND));
        return ResponseEntity.ok().body(medication);
    }

    @Override
    public ResponseEntity <List<Medication>> getAllMedications() {
        List<Medication> medList = medicationRepository.findAll();
        if (medList.isEmpty()) throw new MedicationErrorException(MEDICATION_NOT_AVAILABLE);
        return ResponseEntity.ok().body(medList);
    }

}

package com.adekunleadegoke.droneapi.services;


import com.adekunleadegoke.droneapi.models.Medication;
import com.adekunleadegoke.droneapi.payloads.requests.MedicationRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface MedicationService {
    ResponseEntity<String> registerMedication(MedicationRequest medicationPayload);
    ResponseEntity<Medication> getMedication(Long id);
    ResponseEntity<List<Medication>> getAllMedications();
}
